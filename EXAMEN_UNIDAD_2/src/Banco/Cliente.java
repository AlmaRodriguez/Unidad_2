/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Banco;

import java.util.Scanner;

/**
 *
 * @author ALMA ROSA RODRIGUEZ SANTOS
 */
public class Cliente {
    Scanner input = new Scanner(System.in);
    int opcion;
    Cuenta arregloCuenta[] = new Cuenta[2000];
    boolean salir = false;
    int id,contador;
    double total;

    public void menu() {
        System.out.print("Introduzca nombre del titular: ");
        String titular = input.nextLine();
        do {
            System.out.println("1 para Crear Cuenta.\n2 para Retiro.\n3 para Deposito.\n4 para mostrar Saldo\n5 para Salir");
            opcion = input.nextInt();
            switch (opcion) {
                case 1:
                    contador++;
                    System.out.println("ID DE CUENTA "+contador);
                    System.out.print("Introduzca saldo: ");
                    double saldo = input.nextDouble();
                    arregloCuenta[contador] = new Cuenta(titular, saldo);
                    break;

                case 2:

                    System.out.print("Introduzca ID de cuenta: ");
                    id = input.nextInt();
                    System.out.print("Introduzca cantidad a retirar: ");
                    double retiro = input.nextDouble();
                    arregloCuenta[id].retiro(retiro);
                    if(arregloCuenta[id].getSaldo() > retiro){
                        System.out.println("Retiró de la cuenta: " + retiro + ".\nEl nuevo saldo es de: " + arregloCuenta[id].getSaldo());
                    }
                    break;

                case 3:

                    System.out.print("Introduzca numero de cuenta: ");
                    id = input.nextInt();
                    System.out.print("Introduzca cantidad a depositar: ");
                    double deposito = input.nextDouble();
                    arregloCuenta[id].deposito(deposito);
                    System.out.println("Depositó a la cuenta: " + deposito + ".\nEl nuevo saldo es de: " + arregloCuenta[id].getSaldo());
                    break;

                case 4:
                    System.out.print("Introduzca numero de cuenta: ");
                    id = input.nextInt();
                    System.out.print("SU SALDO ACTUAL ES DE: ");
                    System.out.println(arregloCuenta[id].getSaldo());
                    break;

                case 5:
                    salir = true;
                    break;                
            }
        } while (salir == false);   
        for (int i = 1; i <= contador; i++) {
            total += arregloCuenta[i].getSaldo();
            System.out.println("CUENTA: "+i+"= $"+arregloCuenta[i].getSaldo());            
        }
        System.out.println("SU SALDO ACTUAL DEL BANCO ES: "+total);
    }
    
}
