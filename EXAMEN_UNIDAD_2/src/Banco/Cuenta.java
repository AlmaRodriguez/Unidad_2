/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Banco;
import java.util.Scanner;

/**
 *
 * @author ALMA ROSA RODRIGUEZ SANTOS
 */
public class Cuenta {
    private String titular;
    private double saldo;

    public Cuenta(String titular, double saldo) {
        this.titular = titular;
        this.saldo = saldo;
    }
    
    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
       
    public void retiro(double monto) {
        if(saldo<monto){
            System.out.println("NO HAY FONDOS SUFICIENTES SALDO ACTUAL $"+saldo);
        }else{
            this.saldo = saldo - monto;
        }
    }
    
    public void deposito(double monto) {
        this.saldo = saldo + monto;
    }
    
}
